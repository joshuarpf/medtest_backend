<?php
 namespace App\Helpers;

use DateTime;

class ForecastHelper {

  // Number of ram per study
  private const RAM_PER_STUDY = 2;

  // Cost of RAM per hour in MB
  private const RAM_PER_HOUR = ( 0.00553 / 1000 );

  // Storage consumption per study in mb
  private const STORAGE_PER_STUDY = 10;

  // Storage cost per MB in a month
  private const STORAGE_COST_PER_MONTH = ( 0.10 / 1000 ); 

  private static $forecast = [];

  /**
   * Main method that executes the process of calculations
   *
   * @param [type] $monthToForecast
   * @param [type] $studiesPerDay
   * @param [type] $studyGrowth
   * @return void
   */
  public static function calculate($monthToForecast, $studiesPerDay, $studyGrowth) {
    self::setMonthCollection($monthToForecast);

    if ($monthToForecast > 0) {
      if ($studiesPerDay > 0) {
        self::calculateForNumberOfStudies($studiesPerDay, $studyGrowth);
      }
      
      self::calculateForCostForRAM();
      self::calculateForCostForStorage();
      self::calculateForTotalCost();  
    }
    
    return self::$forecast;
  }

  /**
   * Calculates for the RAM cost per month
   *
   * @return void
   */
  private static function calculateForCostForRAM() {
    foreach(self::$forecast as $month) {
      if ( $month->number_of_studies ) {
        $totalRam = ( $month->number_of_studies * self::RAM_PER_STUDY );
        $month->RAM_cost = ($totalRam * self::RAM_PER_HOUR);
      }
    }
  }

  /**
   * Calculates for the cost of storage per month
   *
   * @return void
   */
  private static function calculateForCostForStorage()
  {
    foreach(self::$forecast as $month) {
      if ($month->number_of_studies && $month->number_of_studies > 0) {
        $storage = ($month->number_of_studies * self::STORAGE_PER_STUDY);
        $month->storage_cost = ($storage * self::STORAGE_COST_PER_MONTH);
      }
    }
  }

  /**
   * Populates the forecast collection with the calculate number of studies per month
   *
   * @param [type] $studiesPerday
   * @param [type] $studyGrowth
   * @return void
   */
  private static function calculateForNumberOfStudies($studiesPerDay, $studyGrowth) {
    foreach(self::$forecast as $month) {
      $daysInMonth = self::getDaysInAMonth($month->dateYear, $month->dateMonth);
      $studiesInAMonth = self::getTotalNumberOfStudies($studiesPerDay, $daysInMonth);
      if ($studyGrowth && $studyGrowth > 0) {
        $percentage = self::getPercentageFromNumber($studiesInAMonth, $studyGrowth);
      } else {
        $percentage = 0;
      }

      $month->number_of_studies = $studiesInAMonth + $percentage;
    }
  }

  private static function calculateForTotalCost()
  {
    foreach(self::$forecast as $month) {
      if ($month->RAM_cost && $month->storage_cost) {
        $month->total_cost = ($month->RAM_cost + $month->storage_cost);
      }
    }
  }

  /**
   * Returns the number of days in a given month
   *
   * @param [type] $year
   * @param [type] $month
   * @return void
   */
  private static function getDaysInAMonth($year, $month) {
    return round((mktime(0, 0, 0, $month+1, 1, $year) - mktime(0, 0, 0, $month, 1, $year)) / 86400);
  }

  /**
   * Returns an array collection that represents the forecast where each element is a month
   *
   * @param [type] $monthToForecast
   * @return void
   */
  private static function setMonthCollection($monthToForecast) {
    
    $dateToManipulate = new DateTime("now");

    for($counter = 1; $counter <= $monthToForecast; $counter++) {
      $month              = new \stdClass;

      $month->name        = $dateToManipulate->format("M").' '.$dateToManipulate->format("Y");
      $month->dateMonth   = $dateToManipulate->format("m");
      $month->dateYear    = $dateToManipulate->format("Y");
      
      array_push(self::$forecast, $month);

      $dateToManipulate->modify('+1 month');
    }
  }

  /**
   * Reurns the percentage given from a given number
   *
   * @param [type] $number
   * @param [type] $percent
   * @return void
   */
  private static function getPercentageFromNumber($number, $percent) {
    return ($percent / 100) * $number;
  }

  /**
   * Returns the total number of studies given the number of studies per day and number of days
   *
   * @param [type] $studiesPerday
   * @param [type] $numberOfDays
   * @return void
   */
  private static function getTotalNumberOfStudies($studiesPerday, $numberOfDays) {
    return ($studiesPerday * $numberOfDays);
  }
 }