<?php

namespace App\Helpers;

class CommonHelper {

  /**
   * Wrapper to return the response
   *
   * @param [type] $status
   * @param [type] $data
   * @return void
   */
  public static function returnResponse($status, $data) {
    $returnObject = new \stdClass;

    $returnObject->status = $status;
    $returnObject->data = $data; 

    return response()->json($returnObject, $status);
  }
}