<?php

namespace App\Http\Controllers\API;

use App\Helpers\CommonHelper;
use App\Helpers\ForecastHelper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ForecastController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
      if ( $this->areInputsValid($request) ) {
        $monthToForecast  = $request->month_to_forecast;
        $studiesPerday    = $request->studies_per_day;
        $studyGrowth      = $request->study_growth;

        $calculations     = ForecastHelper::calculate($monthToForecast, $studiesPerday, $studyGrowth); 

        if ($calculations != null) {
          return CommonHelper::returnResponse(200, $calculations);
        } else {
          return CommonHelper::returnResponse(202, null);
        }
      } else {
        return CommonHelper::returnResponse(400, "invalid input");
      }
    }

    /**
     * Method to execute a validation of inputs
     *
     * @param [type] $request
     * @return void
     */
    private function areInputsValid($request) {
      $valid = true; 

      $validator = Validator::make($request->all(), [
        'month_to_forecast' => 'required|numeric',
        'studies_per_day' => 'required|numeric',
        'study_growth' => 'required|numeric',
      ]);

      if ($validator->fails()) {
        $valid = false;
      }

      return $valid;
    }
}
